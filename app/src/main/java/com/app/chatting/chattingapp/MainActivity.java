package com.app.chatting.chattingapp;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;

import com.app.chatting.chattingapp.fragment.AboutAppFragment;
import com.app.chatting.chattingapp.fragment.AddGroupFragment;
import com.app.chatting.chattingapp.fragment.ContactFragment;
import com.app.chatting.chattingapp.fragment.HomeFragment;
import com.app.chatting.chattingapp.fragment.InviteFreindFragment;
import com.app.chatting.chattingapp.fragment.SettingFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView mNavigationView;
    private FragmentTransaction fragmentTransaction;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find toolbar in view layout
        mToolbar = findViewById(R.id.toolbar);
        //promote toolbar as actionbar
        setSupportActionBar(mToolbar);


        //find drawer
        mDrawer = findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        //set main fragment as default fragment
        fragment = new HomeFragment();
        fragmentManager =getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_fragment,fragment).commit();

    }
//
//    private void setupDrawerContent(NavigationView mNavigationView) {
//        mNavigationView.setNavigationItemSelectedListener(
//                new NavigationView.OnNavigationItemSelectedListener() {
//                    @Override
//                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                        selectDrawerItem(item);
//                        return true;
//                    }
//                });
//    }
//
//    private void selectDrawerItem(MenuItem item) {
//        Fragment fragment = null;
//        Class fragmentClass = null;
//        switch (item.getItemId()) {
//            case R.id.home_activity:
//                fragmentClass = HomeFragment.class;
//                break;
//            case R.id.add_new_group:
//                fragmentClass = AddGroupFragment.class;
//                break;
//            case R.id.contacts:
//                fragmentClass = ContactFragment.class;
//                break;
//            case R.id.invite_friend:
//                fragmentClass = InviteFreindFragment.class;
//                break;
//            case R.id.setting:
//                fragmentClass = SettingFragment.class;
//                break;
//            case R.id.about:
//                fragmentClass = AboutAppFragment.class;
//                break;
//            default:
//                fragmentClass = HomeFragment.class;
//                break;
//        }
//
//        try {
//            fragment = (Fragment) fragmentClass.newInstance();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment).commit();
//
//        item.setChecked(true);
//        setTitle(item.getTitle());
//        mDrawer.closeDrawers();
//
//
//    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        //to toggle Navigation view
        return new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_new_group:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (item.getItemId()) {
            case R.id.home_activity:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.add_new_group:
                fragmentClass = AddGroupFragment.class;
                break;
            case R.id.contacts:
                fragmentClass = ContactFragment.class;
                break;
            case R.id.invite_friend:
                fragmentClass = InviteFreindFragment.class;
                break;
            case R.id.setting:
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.about:
                fragmentClass = AboutAppFragment.class;
                break;
            default:
                fragmentClass = HomeFragment.class;
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment).commit();

        item.setChecked(true);
        setTitle(item.getTitle());
        mDrawer.closeDrawers();


        return true;

    }
}
