package com.app.chatting.chattingapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.chatting.chattingapp.adapter.ChatAdapter;
import com.app.chatting.chattingapp.model.Chat;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager rcManager;
    ArrayList<Chat> chatArrayList = new ArrayList<Chat>();
    ImageView imageViewInsertGallary, imageViewSendBtn, imageViewVoiceRecorder;
    ImageView imageViewTest;
    private final int GET_IMAGE_FROM_GALLARY_CODE = 100;
    private BottomSheetDialog mBottomSheetDialog;
    private BottomSheetBehavior mBottomSheetBehavior;

    private SupportMapFragment supportMapFragment;



    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

//        //reference to tool bar
        toolbar = findViewById(R.id.sub_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back));

        addChat();
        //initial recycler view
        recyclerView = findViewById(R.id.rc_chat_container);
        rcManager = new LinearLayoutManager(this);
        rcManager.scrollToPosition(chatArrayList.size() - 1);
        recyclerView.setLayoutManager(rcManager);
        ChatAdapter adapter = new ChatAdapter(this, chatArrayList);
        recyclerView.setAdapter(adapter);
//        rcManager = new LinearLayoutManager(this);

        // click on chat bar item
        imageViewInsertGallary = findViewById(R.id.send_img);
        imageViewSendBtn = findViewById(R.id.send_btn);
        imageViewVoiceRecorder = findViewById(R.id.send_voice);
        imageViewTest = findViewById(R.id.test_image);

        imageViewInsertGallary.setOnClickListener(this);
        imageViewSendBtn.setOnClickListener(this);
        imageViewVoiceRecorder.setOnClickListener(this);


        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

    }

    void addChat() {
        for (int i = 0; i < 20; i++) {
            chatArrayList.add(new Chat("Jackie Chan", "Hello " + (i) + " time.", "9:00"));
        }
    }


    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        Class fragmentClass = null;

        switch (v.getId()) {
            case R.id.send_img:
                getDialogMedia();
                break;
            case R.id.send_btn:
                Toast.makeText(this, "Message sent.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.send_voice:
                Toast.makeText(this, "Voice sent", Toast.LENGTH_SHORT).show();
                break;
            case R.id.send_location:
                getDialogMap();
                break;
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void getDialogMap(){
        mBottomSheetDialog = new BottomSheetDialog(ChatActivity.this);
        View parentView = getLayoutInflater().inflate(R.layout.dialog_map, null);
        mBottomSheetDialog.setContentView(parentView);
        mBottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        mBottomSheetBehavior.setPeekHeight(
                (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics()
                ));
        mBottomSheetDialog.show();
    }

    public void getDialogMedia(){
        mBottomSheetDialog = new BottomSheetDialog(ChatActivity.this);
        View parentView = getLayoutInflater().inflate(R.layout.dialog_media, null);
        mBottomSheetDialog.setContentView(parentView);
        mBottomSheetBehavior = BottomSheetBehavior.from((View) parentView.getParent());
        mBottomSheetBehavior.setPeekHeight(
                (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics()
                ));
        mBottomSheetDialog.show();

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("Image/*");
        startActivityForResult(intent, GET_IMAGE_FROM_GALLARY_CODE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Phnom Peng, Cambodia,
        // and move the map's camera to the same location.
        LatLng RUPP = new LatLng(11.569021, 104.890696);
        googleMap.addMarker(new MarkerOptions().position(RUPP)
                .title("Marker in Phnom Penh"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(RUPP));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(RUPP, 17);
        googleMap.animateCamera(cameraUpdate);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==GET_IMAGE_FROM_GALLARY_CODE && resultCode==RESULT_OK){
            Uri selectedImageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                imageViewTest.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}

