package com.app.chatting.chattingapp.model;

import java.util.ArrayList;

/**
 * Created by thydaduong on 1/12/2018.
 */

public class FriendList {
    public static ArrayList<Friend> createFiendList(){
        ArrayList<Friend> friends = new ArrayList<Friend>();

        for (int i= 0; i <=15; i++){
            friends.add(new Friend( "friend " + (++i), "sent a message", "4:40"));
        }

        return friends;
    }
}
