package com.app.chatting.chattingapp.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.chatting.chattingapp.R;
import com.app.chatting.chattingapp.adapter.FriendAdapter;
import com.app.chatting.chattingapp.model.Friend;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.ItemDecoration itemDecoration;
    ArrayList<Friend> friends = new ArrayList<Friend>();
    FloatingActionButton fabAdd;

    public HomeFragment() {
        // Required empty public constructor
    }

    void addData(){

        for (int i= 0; i <=15; i++){
            friends.add(new Friend( "friend " + (i), "sent a message", "4:40"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_home, container, false);

        addData();

        // reference to recycler view
        recyclerView = fragmentView.findViewById(R.id.friend_recycler);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // add divider line between Items
        recyclerView.setAdapter(new FriendAdapter(getActivity(), friends));
        itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        //set on item click
//        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(
//                new ItemClickSupport.OnItemClickListener() {
//                    @Override
//                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                        // do it
//                    }
//                }
//        );
        fabAdd = fragmentView.findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "clicked", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        });


        return fragmentView;
    }

}
