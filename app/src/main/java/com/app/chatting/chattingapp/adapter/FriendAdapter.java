package com.app.chatting.chattingapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.chatting.chattingapp.ChatActivity;
import com.app.chatting.chattingapp.R;
import com.app.chatting.chattingapp.model.Friend;

import java.util.List;

/**
 * Created by thydaduong on 1/12/2018.
 */

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    //store friend in a variable;
    private List<Friend> mFriendList;
    private Context mContext;

    // view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txtUsername;
        public TextView txtLatAction;
        public TextView txtRecievedTime;
        private Context context;


        public ViewHolder(View itemView) {
            super(itemView);
            txtUsername     = itemView.findViewById(R.id.username);
            txtLatAction    = itemView.findViewById(R.id.last_action);
            txtRecievedTime = itemView.findViewById(R.id.last_recieved_time);
            this.context    = context;

            //on click item view in recycler view
            itemView.setOnClickListener(this);
}

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION){
                Friend friend = mFriendList.get(position);
                Intent i = new Intent(getContext(), ChatActivity.class);
                getContext().startActivity(i);
            }
        }
    }

    //pass in the friend array to constructor
    public FriendAdapter(Context context, List<Friend> friends){
        mFriendList = friends;
        mContext    = context;

    }

    private Context getContext(){
        return mContext;
    }

    @Override
    public FriendAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //inflate layout
        View friendView = layoutInflater.inflate(R.layout.friend, parent, false);

        //return a view holder instant
        ViewHolder viewHolder = new ViewHolder(friendView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FriendAdapter.ViewHolder holder, int position) {
        //get data model base on position
        Friend friend = mFriendList.get(position);

        //set item views base on view data and model;
        TextView textName   = holder.txtUsername;
        textName.setText(friend.getUsername());
        TextView textAction = holder.txtLatAction;
        textAction.setText(friend.getLastAction());
        TextView textTime   = holder.txtRecievedTime;
        textTime.setText(friend.getLastRecieveTime());
    }

    //return
    @Override
    public int getItemCount() {
        return mFriendList.size();
    }

}
