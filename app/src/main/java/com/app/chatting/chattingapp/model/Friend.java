package com.app.chatting.chattingapp.model;

/**
 * Created by thydaduong on 1/12/2018.
 */

public class Friend {
    private String username;
    private String lastAction;
    private String lastRecieveTime;

    public Friend(String username, String lastAction, String lastRecieveTime) {
        this.username = username;
        this.lastAction = lastAction;
        this.lastRecieveTime = lastRecieveTime;


    }

    public String getUsername() {
        return username;
    }

    public String getLastAction() {
        return lastAction;
    }

    public String getLastRecieveTime() {
        return lastRecieveTime;
    }
}
