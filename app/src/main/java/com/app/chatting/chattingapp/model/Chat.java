package com.app.chatting.chattingapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thydaduong on 1/17/2018.
 */

public class Chat {
    String chatUser, chatText, chatTime;
    private ArrayList<Chat> chatArrayList = new ArrayList<Chat>();

    public Chat(String chatUser, String chatText, String chatTime) {
        this.chatUser = chatUser;
        this.chatText = chatText;
        this.chatTime = chatTime;
    }

    public String getChatUser() {
        return chatUser;
    }

    public String getChatText() {
        return chatText;
    }

    public String getChatTime() {
        return chatTime;
    }

    public void addChat() {
        for (int i = 0; i < 20; i++) {
            chatArrayList.add(new Chat("Jackie Chan", "Hello "+ i+" time.", "9:00"));
        }
    }

}
