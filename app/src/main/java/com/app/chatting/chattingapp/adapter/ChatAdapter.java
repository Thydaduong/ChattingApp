package com.app.chatting.chattingapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.chatting.chattingapp.R;
import com.app.chatting.chattingapp.model.Chat;

import java.util.List;

/**
 * Created by thydaduong on 1/17/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter {
    public List<Chat> mChats;
    Context mContext;
    private TextView txtViewChatUser, txtViewChatText, txtViewChatTime;
    public static final int VIEW_TYPE_CHAT_SENT = 1;
    public static final int VIEW_TYPE_CHAT_RECIEVED = 2;


    // receive message
    public class ReceivedViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewChatUser, textViewChatText, textViewChatTime;

        public ReceivedViewHolder(View itemView) {
            super(itemView);

            textViewChatUser = itemView.findViewById(R.id.chat_user);
            textViewChatText = itemView.findViewById(R.id.chat_text);
            textViewChatTime = itemView.findViewById(R.id.chat_time);

        }

        void bind(Chat chat) {
            textViewChatUser.setText(chat.getChatUser());
            textViewChatText.setText(chat.getChatText());
            textViewChatTime.setText(chat.getChatTime());
        }

    }

    public class SentViewHolder extends RecyclerView.ViewHolder {
        TextView txtSentText, txtSentTime;

        public SentViewHolder(View itemView) {
            super(itemView);

            txtSentText = itemView.findViewById(R.id.sent_chat_text);
            txtSentTime = itemView.findViewById(R.id.sent_chat_time);
        }

        void bind(Chat chat) {
            txtSentText.setText(chat.getChatText());
            txtSentTime.setText(chat.getChatTime());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

//        View view = layoutInflater.inflate(R.layout.recieved_chat, parent, false);
//
//        ReceivedViewHolder viewHolder = new ReceivedViewHolder(view);
//
//      return viewHolder;
        View view;

        if (viewType == VIEW_TYPE_CHAT_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.sent_chat, parent, false);
            return new SentViewHolder(view);
        } else if (viewType == VIEW_TYPE_CHAT_RECIEVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recieved_chat, parent, false);
            return new ReceivedViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Chat chat = mChats.get(position);

//        txtViewChatUser = holder.textViewChatUser;
//        txtViewChatUser.setText(chat.getChatUser());
//
//        txtViewChatText = holder.textViewChatText;
//        txtViewChatText.setText(chat.getChatText());
//
//        txtViewChatTime = holder.textViewChatTime;
//        txtViewChatTime.setText(chat.getChatTime());

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_CHAT_SENT:
                ((SentViewHolder) holder).bind(chat);
                break;
            case VIEW_TYPE_CHAT_RECIEVED:
                ((ReceivedViewHolder) holder).bind(chat);
        }

    }


    @Override
    public int getItemCount() {
        return mChats.size();
    }

    //pass chat to constructor
    public ChatAdapter(Context context, List<Chat> chats) {
        mContext = context;
        mChats = chats;
    }

    @Override
    public int getItemViewType(int position) {

        if (position % 2 == 0) {
            return VIEW_TYPE_CHAT_SENT;
        }
        return VIEW_TYPE_CHAT_RECIEVED;
    }

}
